#Duck Hunt 
##Unity 3D

Ce programme est un jeu de tir utilisant les Sprites de Duck Hunt, fonctionnant sous la version 2022.2.8f1 de Unity, le jeu fonctionne sous Windows et Android, il à été tester sous Android 13. 

le principe du jeu est simple, quand on lance le jeu on arrive sur la scène 1, qui nous propose de "cliquer" sur un bouton, pour jouer, une fois cela effectué on change de scène, des canards arrivent de gauche vers la droite et il suffit de cliquer dessus pour gagner des points, ou toucher l'écran tactile. Il y a 3 types de canard, les vert sont les plus lents et donne le moins de point, puis les bleu, plus rapide et donne plus de points, et les rouges qui sont les plus rapides et donne le plus de point, les box collider de chaque canard sont different en fonction de leur couleur, ainsi le box collider d'un canard vert sera légèrement plus grand que celui d'un canard rouge, ceci est pour augmenter légèrement la difficulté. 
Lorsque 100 canards sont créés la partie est fini et on arrive sur la scène de fin qui nous donne notre score et notre meilleur score, cette scene permet aussi de relancer une partie. 

mes principales difficultées étais sur le score, le score est donc enregistrer dans une variable public que j'utilise dans plusieurs script de la scene 2 et j'utilise la méthode PlayerPref pour la récupérer dans la scene 3. une deuxieme difficulté que j'ai rencontré à été l'utilisation de delay, qui dans Unity est assez spécifique et ma obliger à créé certaines fonction a part, par exemple lorsque l'on clique sur un canard 

lien gitlab du projet : https://gitlab.com/3emeoeil/duck-hunt-unity